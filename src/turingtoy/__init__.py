from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:

    blank = machine["blank"]
    move_history = []
    step_do = 0
    position = 0
    inputed_list: list[str] = list(input_)
    actual_state = machine["start state"]

    while (actual_state not in machine["final states"]) or (
        steps is not None and step_do <= steps
    ):
        if position < 0:
            inputed_list.insert(0, blank)
            position = 0
        elif position >= len(inputed_list):
            inputed_list.append(blank)

        number = inputed_list[position]
        direction = machine["table"][actual_state][number]

        history_add = {
            "state": actual_state,
            "read": number,
            "position": position,
            "memory": "".join(inputed_list),
            "direction": direction,
        }

        move_history.append(history_add)

        if direction == "L":
            position -= 1
        elif direction == "R":
            position += 1
        else:
            write = direction.get("write")
            go_left = direction.get("L")
            go_right = direction.get("R")

            if write is not None:
                inputed_list[position] = write

            if go_left is not None:
                position -= 1
                actual_state = go_left
            elif go_right is not None:
                position += 1
                actual_state = go_right
            else:
                position += 0
                break

        step_do += 1

    result = "".join(inputed_list).strip(blank)
    if actual_state in machine["final states"]:
        accepted = True
    else:
        accepted = False
    return (result, move_history, accepted)
